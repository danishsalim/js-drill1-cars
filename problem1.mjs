
const problem1 = (inventory, id) => {
  if (inventory instanceof Array && typeof id == "number") {
    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].id == id) {
        return inventory[index];
      }
    }
  } else {
    return [];
  }
};


export default problem1;
