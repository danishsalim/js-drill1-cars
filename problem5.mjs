const problem5 = (years) => {
  if (years instanceof Array && years.length) {
    let olderYears = [];
    for (let year of years) {
      if (year > 2000) {
        olderYears.push(year);
      }
    }
    return olderYears;
  } else {
    return [];
  }
};

export default problem5;
