const problem6 = (inventory) => {
  if (inventory instanceof Array && inventory.length) {
    let BMWAndAudi = [];
    for (let car of inventory) {
      if (car.car_make == "Audi" || car.car_make == "BMW") {
        BMWAndAudi.push(car);
      }
    }
    return BMWAndAudi;
  } else {
    return [];
  }
};

export default problem6;
