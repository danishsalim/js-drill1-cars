const problem4 = (inventory) => {
  if (inventory instanceof Array && inventory.length) {
    let carYears = [];
    for (let car of inventory) {
      carYears.push(car.car_year);
    }
    return carYears;
  } else {
    return [];
  }
};

export default problem4;
