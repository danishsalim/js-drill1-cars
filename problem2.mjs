const problem2 = (inventory) => {
  if (inventory instanceof Array  && inventory.length) {
    return inventory[inventory.length - 1];
  } else {
    return [];
  }
};


export default problem2;
