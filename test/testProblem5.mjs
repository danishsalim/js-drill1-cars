import { inventory } from "../inventory.mjs";
import problem4 from "../problem4.mjs";
import problem5 from "../problem5.mjs";

let carYears = problem4(inventory);
let carsOlderthan2000 = problem5(carYears);
console.log(carsOlderthan2000.length);
