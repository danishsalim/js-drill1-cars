import { inventory } from "../inventory.mjs";
import problem2 from "../problem2.mjs";


const lastCar = problem2(inventory);

console.log("Last car is a " + lastCar.car_make + " " + lastCar.car_model);
