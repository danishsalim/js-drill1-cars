import { inventory } from "../inventory.mjs";
import problem1 from "../problem1.mjs";

const carDetail = problem1(inventory, 33);

console.log(
  "car 33 is a " +
    carDetail.car_year +
    " " +
    carDetail.car_make +
    " " +
    carDetail.car_model
);
