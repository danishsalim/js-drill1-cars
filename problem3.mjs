
const problem3 = (inventory) => {
  if (inventory instanceof Array && inventory.length) {
    let carModels = [];
    for (let index = 0; index < inventory.length; index++) {
      carModels.push(inventory[index].car_model);
    }

    for (let i = 0; i < carModels.length - 1; i++) {
      for (let j = 0; j < carModels.length-1-i; j++) {
        if (carModels[j] > carModels[j+1]) {
          //swap car models
          let temp = carModels[j];
          carModels[j] = carModels[j+1];
          carModels[j+1] = temp;
        }
      }
    }

    return carModels

  } else {
    return [];
  }
};

export default problem3;
